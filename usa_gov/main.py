from pandas import DataFrame, Series
import matplotlib.pyplot as plt
import pandas as pd
import json
from utils.chart import chartParams
# import sys
# sys.path.insert(1, '/utils')


# Counting time-zones with Pandas.

records = [json.loads(line) for line in open('./usa_gov/data_source.txt')]

frame = DataFrame(records)

timeZoneCount = frame['tz'].value_counts()
# Getting top 10 time-zones.
print(timeZoneCount[:10])

# Cleaning up the data set by filling unkown and missing values.
cleanTimeZone = frame['tz'].fillna('Missing')
cleanTimeZone[cleanTimeZone == ''] = 'Unknown'

timeZoneCount = cleanTimeZone.value_counts()
print(timeZoneCount[:10])
# Top 10 access chart
plt.figure(0)

timeZoneAxes = timeZoneCount[:10].plot(
    **chartParams('barh', 'Top time zones in the usa.gov sample data', '#00BEC5'))
# plt.show()
# frame['a'][0]  ->  Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKi...
# Info about browser, device and application used to perform the URL shortening.
timeZoneAxes.set_xlabel('Access')

browserCount = Series([x.split()[0]
                       for x in frame['a'].dropna()]).value_counts()[:10]
plt.figure(1)
browserCount.plot(
    **chartParams('bar', 'Access according browser, device and application'))
print(browserCount)

plt.show()
