# Sets up the chart's params
def chartParams(kind='bar', title='Your title', color='#27A9E1'):
    return {'kind': kind, 'title': title, 'color': color}
