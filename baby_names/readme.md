# Baby names 1880 - 2010

The United States Social Security Administration (SSA) made available data on the frequency of baby names from 1880 to 2010. This section shows some data analysis related to this data, such as:

- Visualize the proportion of babies given a particular name;
- Determine the relative rank of a name;
- Determine the most popular names in each year or the names with largest increases or decreases;
- Analyze trends in names: vowels, consonants, lenght, overall diversitym changes in spelling, first and last letters;
- Analyze external sources of trends: biblical names, celebrities, demographic cnages.

The raw data structure looks like:

```
        name sex  births  year
0       Mary   F    7065  1880
1       Anna   F    2604  1880
2       Emma   F    2003  1880
3  Elizabeth   F    1939  1880
4     Minnie   F    1746  188
```

## Data plotting

This section shows some data plotting for some analysis related to baby names.

The data on births by sex over the years can be seen in Figure 1.

![alt text](assets/total_births_by_sex_and_year.png "Total births by sex and year")

**Figure 1** - Total births by sex and year.

### Analyzing Naming Trends

Figure 2 shows a trend for names `John`, ``Harry`, `Mary` and `Marilyn` over the years.

![alt text](assets/number_of_births_per_year.png "Number of births per year")
**Figure 2** - Most 1000 popular names frequency over time.