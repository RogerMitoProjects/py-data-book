# Counting time-zones with Pandas

Info about browser, device, or application used to perform the URL shortening.

<img src="./means-of-access.png" width="300">

Top time zones in the 1.usa.gov sample data.

<img src="./top-access.png" width="300">
