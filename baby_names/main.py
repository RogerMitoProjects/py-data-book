import pandas as pd
import matplotlib.pyplot as plt


def add_proportion(group: pd.DataFrame):
    """Add proportion for each name acorrding to its ocurrence."""
    # Ref.: https://www.w3schools.com/python/pandas/ref_df_astype.asp
    births = group.births.astype(float)

    group['prop'] = births / births.sum()
    return group


def get_top1000(group: pd.DataFrame, sort_by: str):
    """Get top 1000 values based on sort_by value"""
    return group.sort_values(by=sort_by, ascending=False).head(1000)

# 2010 is the last available year
years = range(1880, 2011)

pieces = []
columns = ['name', 'sex', 'births']

for year in years:
    path = 'datasets/yob%d.txt' % year
    frame = pd.read_csv(path, names=columns)

    frame['year'] = year
    pieces.append(frame)


# Merge all frames into a single DataFrame
names = pd.concat(pieces, ignore_index=True)
print(names.head())
# Data agregation with pivot table
total_births = names.pivot_table(
    values='births', index='year', columns='sex', aggfunc="sum")

# Group by 'name' and sum the births
names_aggregated = names.groupby('name').births.sum().reset_index()
# Get top 1000 names
top1000_names = get_top1000(names_aggregated, 'births')

# Filter data to include only the top 1000 names
filtered_names = names[names['name'].isin(top1000_names['name'])]

total_births_by_name = filtered_names.pivot_table(
    values='births', index='year', columns='name', aggfunc='sum')


# Data plotting
subset = total_births_by_name[['John', 'Harry', 'Mary', 'Marilyn']]
subset.plot(subplots=True, figsize=(12, 10), grid=False,
            title='Number of births per year')

# Total of births by sex and year
total_births.plot(kind='line', title='Total births by sex and year')

plt.show()
